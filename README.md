Made by

Designers:
Niko Rantasalo, Alina Puukko, Henri Leppänen

Lead Programmer:
Henri Leppänen

Programmers:
Ilari Salonen, Aarne Hällstén, Tiina Mannelin, Henri Tainio

Audio:
Henri Tainio

Artists:
Alina Puukko, Tanja Eremich

Writers:
Niko Rantasalo, Ilari Salonen, Tiina Mannelin


The game is made for Android and Windows. You are free to download and build it, but using any of the project's assets is strictly forbidden!